import com.SessionUser;
import com.controller.MainController;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;

import static javafx.application.Application.launch;

/**
 * The type MainApp.
 */
public class MainApp extends Application {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Instanciation du controller Login
     *
     * @param stage initialize stage
     */
    @Override
    public void start(Stage stage) throws Exception {
        URL location = getClass().getResource("/views/login.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = fxmlLoader.load(location.openStream());
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.getIcons().add(new Image(this.getClass().getResourceAsStream("img/icon.png")));
        stage.setOnCloseRequest(e -> onClose(stage));

//        final ObservableList<String> stylesheets = scene.getStylesheets();
//        stylesheets.add(MainApp.class.getResource("/css/mainStyle.css").toExternalForm());

        MainController mainController = fxmlLoader.getController();
        mainController.setCurrentStage(stage);
        mainController.showStage();
    }

    public void onClose(Stage stage){
        JFXAlert alert = new JFXAlert(stage);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setOverlayClose(false);
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label("Le programme se ferme"));
        
        JFXButton closeButton = new JFXButton("OK");
        closeButton.getStyleClass().add("dialog-accept");
        closeButton.setOnAction(event -> alert.hideWithAnimation());

        layout.setActions(closeButton);
        alert.setContent(layout);
        SessionUser.logout();
        alert.showAndWait()
                .filter(response -> response == closeButton)
                .ifPresent(response -> stage.close());
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

}
