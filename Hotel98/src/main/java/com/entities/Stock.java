package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Stock{

    private Integer id;
    private String name;
    private Integer idEmployee;
    private Double quantity;
    private Integer idUnit;

    public Stock(){}
    public Stock(Integer idS, String nameS, Integer idEmployeeS, Integer idUnitS, Double quantity){
        this.id = idS;
        this.name = nameS;
        this.idEmployee = idEmployeeS;
        this.idUnit = idUnitS;
        this.quantity = quantity;
    }
    public Stock(String nameS, Integer idEmployeeS, Integer idUnitS, Double quantity){
        this.name = nameS;
        this.idEmployee = idEmployeeS;
        this.idUnit = idUnitS;
        this.quantity = quantity;
    }
    public Stock(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.name = rset.getString("name");
        this.idEmployee = rset.getInt("id_employee");
        this.idUnit = rset.getInt("id_unit") ;
        this.quantity = rset.getDouble("quantity");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }

    public Integer getIdUnit() {
        return idUnit;
    }

    public void setIdUnit(Integer idUnit) {
        this.idUnit = idUnit;
    }
    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
