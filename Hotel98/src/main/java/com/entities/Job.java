package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Job {

    private Integer id;
    private String nameJob;
    private Integer service;

    public Job(){}
    public Job(Integer idJ, String nameJobJ, Integer serviceJ){
        this.id = idJ;
        this.nameJob = nameJobJ;
        this.service = serviceJ;
    }
    public Job(String nameJobJ, Integer serviceJ){
        this.nameJob = nameJobJ;
        this.service = serviceJ;
    }
    public Job(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.nameJob = rset.getString("name_job");
        this.service = rset.getInt("id_service");
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameJob() {
        return nameJob;
    }

    public void setNameJob(String nameJob) {
        this.nameJob = nameJob;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }
}
