package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Service {
    private Integer id;
    private String nameService;

    public Service(){}
    public Service(Integer idS, String nameServiceS){
        this.id = idS;
        this.nameService = nameServiceS;
    }
    public Service(String nameServiceS){
        this.nameService = nameServiceS;
    }
    public Service(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.nameService = rset.getString("name_service");
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }
}

