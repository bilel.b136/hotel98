package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Ticket {

    private Integer id;
    private String objectTicket;
    private String subjectTicket;
    private Integer idParentTicket;

    public Ticket(){}
    public Ticket(Integer idT, String objectTicketT, String subjectTicketT, Integer idPT){
        this.id = idT;
        this.objectTicket = objectTicketT;
        this.subjectTicket = subjectTicketT;
        this.idParentTicket = idPT;
    }
    public Ticket(String objectTicketT, String subjectTicketT, Integer idPT){
        this.objectTicket = objectTicketT;
        this.subjectTicket = subjectTicketT;
        this.idParentTicket = idPT;
    }
    public Ticket(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.objectTicket = rset.getString("object_ticket");
        this.subjectTicket = rset.getString("subject_ticket");
        this.idParentTicket = rset.getInt("id_parent_ticket");
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObjectTicket() {
        return objectTicket;
    }

    public void setObjectTicket(String objectTicket) {
        this.objectTicket = objectTicket;
    }

    public String getSubjectTicket() {
        return subjectTicket;
    }

    public void setSubjectTicket(String subjectTicket) {
        this.subjectTicket = subjectTicket;
    }

    public Integer getIdParentTicket() {
        return idParentTicket;
    }

    public void setIdParentTicket(Integer idParentTicket) {
        this.idParentTicket = idParentTicket;
    }
}
