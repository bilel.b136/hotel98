package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class BookingUnexpected {

    private Integer id;
    private String name;
    private String description;
    private String state;
    private Double price;
    private Date created;
    private Integer id_stay;

    public BookingUnexpected(){}
    public BookingUnexpected(Integer idB, String nameB, String descriptionB, String stateB, Double priceB, Date createdB, Integer idStayB){
        this.id = idB;
        this.name = nameB;
        this.description = descriptionB;
        this.state = stateB;
        this.price = priceB;
        this.created = createdB;
        this.id_stay = idStayB;
    }
    public BookingUnexpected(String nameB, String descriptionB, String stateB, Double priceB, Date createdB, Integer idStayB){
        this.name = nameB;
        this.description = descriptionB;
        this.state = stateB;
        this.price = priceB;
        this.created = createdB;
        this.id_stay = idStayB;
    }
    public BookingUnexpected(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.name = rset.getString("name");
        this.description = rset.getString("description");
        this.state = rset.getString("state");
        this.price = rset.getDouble("price");
        this.created = rset.getDate("created");
        this.id_stay = rset.getInt("id_stay");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getId_stay() {
        return id_stay;
    }

    public void setId_stay(Integer id_stay) {
        this.id_stay = id_stay;
    }
}
