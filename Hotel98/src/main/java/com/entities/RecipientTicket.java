package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RecipientTicket {

    private Integer id;
    private Integer idTicket;

    public RecipientTicket(Integer idR, Integer idTicketR){
        this.id = idR;
        this.idTicket = idTicketR;
    }
    public RecipientTicket(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.idTicket = rset.getInt("id_ticket");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(Integer idTicket) {
        this.idTicket = idTicket;
    }
}
