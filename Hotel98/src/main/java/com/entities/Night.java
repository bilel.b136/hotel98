package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Night {

    private Integer id;
    private Date dateNight;
    private Integer idRoom;
    private boolean state;
    private Integer idBreakfast;
    private Integer idStay;
    private String libel_breakfast;

    public Night(){}
    public Night(Integer idN, Date dateNightN, Integer idRoomN, boolean stateN, Integer idBreakfastN, Integer idStayN,String libel_breakfast){
        this.id = idN;
        this.dateNight = dateNightN;
        this.idRoom = idRoomN;
        this.state = stateN;
        this.idBreakfast = idBreakfastN;
        this.idStay = idStayN;
        this.libel_breakfast = libel_breakfast;
    }
    public Night(Date dateNightN, Integer idRoomN, boolean stateN, Integer idBreakfastN, Integer idStayN,String libel_breakfast){
        this.dateNight = dateNightN;
        this.idRoom = idRoomN;
        this.state = stateN;
        this.idBreakfast = idBreakfastN;
        this.idStay = idStayN;
        this.libel_breakfast = libel_breakfast;
    }
    public Night(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.dateNight = rset.getDate("date_night");
        this.idRoom = rset.getInt("id_room");
        this.state = rset.getBoolean("state");
        this.idBreakfast = rset.getInt("id_breakfast");
        this.idStay = rset.getInt("id_stay");
        this.libel_breakfast = rset.getString("libel_breakfast");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateNight() {
        return dateNight;
    }

    public void setDateNight(Date dateNight) {
        this.dateNight = dateNight;
    }

    public Integer getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(Integer idRoom) {
        this.idRoom = idRoom;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Integer getIdBreakfast() {
        return idBreakfast;
    }

    public void setIdBreakfast(Integer idBreakfast) {
        this.idBreakfast = idBreakfast;
    }

    public Integer getIdStay() {
        return idStay;
    }

    public void setIdStay(Integer idStay) {
        this.idStay = idStay;
    }

    public String getLibel_breakfast() {return libel_breakfast;}

    public void setLibel_breakfast(String libel_breakfast) {this.libel_breakfast = libel_breakfast;}
}
