package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ExtTs extends BookingTableSet{

    private Integer id;
    private String phoneNumber;
    private String nameBook;

    public ExtTs(){}
    public ExtTs(Integer idB, Date dateB, String serviceB, String nPhone, String nameB, Integer idEmployee) {
        super(idB, dateB, serviceB, idEmployee);
        this.id = idB;
        this.phoneNumber = nPhone;
        this.nameBook = nameB;
    }
    public ExtTs(Date dateB, String serviceB, String nPhone, String nameB, Integer idEmployee) {
        super(dateB, serviceB, idEmployee);
        this.phoneNumber = nPhone;
        this.nameBook = nameB;
    }
    public ExtTs(ResultSet rset) throws SQLException {
        super(rset);
        this.id = rset.getInt("id");
        this.phoneNumber = rset.getString("phone_number");
        this.nameBook = rset.getString("name_book");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone_number() {
        return phoneNumber;
    }

    public void setPhone_number(String phone_number) {
        this.phoneNumber = phone_number;
    }

    public String getName_book() {
        return nameBook;
    }

    public void setName_book(String name_book) {
        this.nameBook = name_book;
    }
}
