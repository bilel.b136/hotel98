package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class IntSpa extends BookingSpa{

    private Integer id;
    private Integer idStay;

    public IntSpa(){}
    public IntSpa(Integer idI,Integer idSpa, Integer idStayI, Date dateBook, Integer idEmployee){
        super(idSpa, dateBook, idEmployee);
        this.id = idI;
        this.idStay = idStayI;
    }
    public IntSpa(Integer idStayI, Date dateBook, Integer idEmployee){
        super(dateBook, idEmployee);
        this.idStay = idStayI;
    }
    public IntSpa(ResultSet rset) throws SQLException {
        super(rset.getInt("booking_spa.id"), rset.getDate("date_book"), rset.getInt("id_employee"));
        this.id = rset.getInt("id");
        this.idStay = rset.getInt("id_stay");
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdStay() {
        return idStay;
    }

    public void setIdStay(Integer idStay) {
        this.idStay = idStay;
    }
}
