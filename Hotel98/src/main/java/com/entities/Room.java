package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Room {

    private Integer id;
    private Integer numRoom;
    private Double price;
    private Integer idTypeRoom;

    public Room(){}
    public Room(Integer idR, Integer numRoomR, Double priceR, Integer idTypeRoomR){
        this.id = idR;
        this.numRoom = numRoomR;
        this.price = priceR;
        this.idTypeRoom = idTypeRoomR;
    }
    public Room(Integer numRoomR, Double priceR, Integer idTypeRoomR){
        this.numRoom = numRoomR;
        this.price = priceR;
        this.idTypeRoom = idTypeRoomR;
    }
    public Room(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.numRoom = rset.getInt("num_room");
        this.price = rset.getDouble("price");
        this.idTypeRoom = rset.getInt("id_type_room");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumRoom() {
        return numRoom;
    }

    public void setNumRoom(Integer numRoom) {
        this.numRoom = numRoom;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getIdTypeRoom() {
        return idTypeRoom;
    }

    public void setIdTypeRoom(Integer idTypeRoom) {
        this.idTypeRoom = idTypeRoom;
    }
}
