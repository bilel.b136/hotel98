package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Off {

    private Integer id;
    private Date dateStart;
    private Date dateEnd;
    private String description;
    private Integer idRoom;

    public Off(){}
    public Off(Integer idO, Date dateStartO, Date dateEndO, String descriptionO, Integer idRoomO){
        this.id = idO;
        this.dateStart = dateStartO;
        this.dateEnd = dateEndO;
        this.description = descriptionO;
        this.idRoom = idRoomO;
    }
    public Off(Date dateStartO, Date dateEndO, String descriptionO, Integer idRoomO){
        this.dateStart = dateStartO;
        this.dateEnd = dateEndO;
        this.description = descriptionO;
        this.idRoom = idRoomO;
    }
    public Off(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.dateStart = rset.getDate("date_start");
        this.dateEnd = rset.getDate("date_end");
        this.description = rset.getString("description");
        this.idRoom = rset.getInt("id_room");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(Integer idRoom) {
        this.idRoom = idRoom;
    }
}
