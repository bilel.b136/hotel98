package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class IntTs extends BookingTableSet{

    private Integer id;
    private Integer idStay;

    public IntTs(){};
    public IntTs(Integer idI, Integer idTs, Date dateBook , String service, Integer id_employee, Integer idStayI){
        super(idTs,dateBook,service,id_employee);
        this.id = idI;
        this.idStay = idStayI;
    }
    public IntTs(Date dateBook , String service, Integer id_employee, Integer idStayI){
        super(dateBook,service,id_employee);
        this.idStay = idStayI;
    }
    public IntTs(ResultSet rset) throws SQLException {
        super(rset.getInt("booking_table_set.id"),rset.getDate("date_book"),rset.getString("service"),rset.getInt("id_employee"));
        this.id = rset.getInt("id");
        this.idStay = rset.getInt("id_stay");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdStay() {
        return idStay;
    }

    public void setIdStay(Integer idStay) {
        this.idStay = idStay;
    }
}