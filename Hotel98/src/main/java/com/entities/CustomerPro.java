package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class CustomerPro extends Customer{

    private Integer id;
    private Integer idCompany;

    public CustomerPro(){}
    public CustomerPro(Integer idCP, Integer idCompanyCP, Integer idC, String nameC, String firstNameC, String phoneNumberC, Integer postalCodeC, String addressC, Date dateC, Integer idStayC){
        super(idC,nameC,firstNameC,phoneNumberC,postalCodeC,addressC,dateC,idStayC);
        this.id = idCP;
        this.idCompany = idCompanyCP;
    }
    public CustomerPro(Integer idCompanyCP,String nameC, String firstNameC, String phoneNumberC, Integer postalCodeC, String addressC, Date dateC, Integer idStayC){
        super(nameC,firstNameC,phoneNumberC,postalCodeC,addressC,dateC,idStayC);
        this.idCompany = idCompanyCP;
    }
    public CustomerPro(ResultSet rset) throws SQLException {
        super(rset.getInt("customer.id"),rset.getString("name"),rset.getString("first_name"),
                rset.getString("phone_number"),rset.getInt("postal_code"),rset.getString("address"),
                rset.getDate("date_book"),rset.getInt("id_stay"));
        this.id = rset.getInt("id");
        this.idCompany = rset.getInt("id_company");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Integer idCompany) {
        this.idCompany = idCompany;
    }
}
