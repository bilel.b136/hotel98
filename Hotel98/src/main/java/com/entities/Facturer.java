package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Facturer {

    private Integer id;
    private Integer idFacture;

    public Facturer(Integer idF, Integer idFactureF){
        this.id = idF;
        this.idFacture = idFactureF;
    }
    public Facturer(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.idFacture = rset.getInt("id_facture");
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFacture() {
        return idFacture;
    }

    public void setIdFacture(Integer idFacture) {
        this.idFacture = idFacture;
    }
}
