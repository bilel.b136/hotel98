package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Customer {

    private Integer id;
    private String name;
    private String firstName;
    private String phoneNumber;
    private Integer postalCode;
    private String address;
    private Date date;
    private Integer idStay;

    public Customer(){}
    public Customer(Integer idC, String nameC, String firstNameC, String phoneNumberC, Integer postalCodeC, String addressC, Date dateC, Integer idStayC){
        this.id = idC;
        this.name = nameC;
        this.firstName = firstNameC;
        this.phoneNumber = phoneNumberC;
        this.postalCode = postalCodeC;
        this.address = addressC;
        this.date = dateC;
        this.idStay = idStayC;
    }
    public Customer(String nameC, String firstNameC, String phoneNumberC, Integer postalCodeC, String addressC, Date dateC, Integer idStayC){
        this.name = nameC;
        this.firstName = firstNameC;
        this.phoneNumber = phoneNumberC;
        this.postalCode = postalCodeC;
        this.address = addressC;
        this.date = dateC;
        this.idStay = idStayC;
    }
    public Customer(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.name = rset.getString("name");
        this.firstName = rset.getString("first_name");
        this.phoneNumber = rset.getString("phone_number");
        this.postalCode = rset.getInt("postal_code");
        this.address = rset.getString("address");
        this.date = rset.getDate("date_book");
        this.idStay = rset.getInt("id_stay");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdStay() {
        return idStay;
    }

    public void setIdStay(Integer idStay) {
        this.idStay = idStay;
    }
}
