package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Unit {

    private Integer id;
    private String name;

    public Unit(){}
    public Unit(Integer idU, String nameU){
        this.id = idU;
        this.name = nameU;
    }
    public Unit(String nameU){
        this.name = nameU;
    }
    public Unit(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.name = rset.getString("name");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
