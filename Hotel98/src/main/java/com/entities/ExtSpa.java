package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExtSpa {

    private Integer id;
    private String phoneNumber;
    private String nameBook;

    public ExtSpa(){}
    public ExtSpa(Integer idE, String phoneNumberE, String nameBookE){
        this.id = idE;
        this.phoneNumber = phoneNumberE;
        this.nameBook = nameBookE;
    }
    public ExtSpa(String phoneNumberE, String nameBookE){
        this.phoneNumber = phoneNumberE;
        this.nameBook = nameBookE;
    }
    public ExtSpa(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.phoneNumber = rset.getString("phone_number");
        this.nameBook = rset.getString("name_book");

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }
}
