package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateTicket {

    private Integer id;
    private Integer idEmployee;

    public CreateTicket(){}
    public CreateTicket(Integer idC,Integer idEmployeeC){
        this.id = idC;
        this.idEmployee = idEmployeeC;
    }
    public CreateTicket(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.idEmployee = rset.getInt("id_employee");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }
}
