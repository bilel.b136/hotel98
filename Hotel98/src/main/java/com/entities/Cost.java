package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Cost {

    private Integer id;
    private String name;
    private Double price;
    private Integer idService;

    public Cost(){}
    public Cost(Integer idC, String nameC, Double priceC, Integer idServiceC){
        this.id = idC;
        this.name = nameC;
        this.price = priceC;
        this.idService = idServiceC;
    }
    public Cost(String nameC, Double priceC, Integer idServiceC){
        this.name = nameC;
        this.price = priceC;
        this.idService = idServiceC;
    }
    public Cost(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.name = rset.getString("name");
        this.price = rset.getDouble("price");
        this.idService = rset.getInt("id_service");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getIdService() {
        return idService;
    }

    public void setIdService(Integer idService) {
        this.idService = idService;
    }
}
