package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Facture {

    private Integer id;
    private Double price;
    private Date dateFacture;

    public Facture(){}
    public Facture(Integer idF, Double priceF, Date dateFactureF){
        this.id = idF;
        this.price = priceF;
        this.dateFacture = dateFactureF;
    }
    public Facture(Double priceF, Date dateFactureF){
        this.price = priceF;
        this.dateFacture = dateFactureF;
    }
    public Facture(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.price = rset.getDouble("price");
        this.dateFacture = rset.getDate("date_facture");
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(Date dateFacture) {
        this.dateFacture = dateFacture;
    }
}
