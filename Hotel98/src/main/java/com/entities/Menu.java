package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Menu {

    private Integer id;
    private String nameMenu;
    private String starter;
    private String mainCours;
    private String dessert;
    private Double price;

    public Menu(){}
    public Menu(Integer idM, String nameMenuM, String starterM, String mainCoursM, String dessertM, Double priceM){
        this.id = idM;
        this.nameMenu = nameMenuM;
        this.starter = starterM;
        this.mainCours = mainCoursM;
        this.dessert = dessertM;
        this.price = priceM;
    }
    public Menu(String nameMenuM, String starterM, String mainCoursM, String dessertM, Double priceM){
        this.nameMenu = nameMenuM;
        this.starter = starterM;
        this.mainCours = mainCoursM;
        this.dessert = dessertM;
        this.price = priceM;
    }
    public Menu(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.nameMenu = rset.getString("name_menu");
        this.starter = rset.getString("starter");
        this.mainCours = rset.getString("main_cours");
        this.dessert = rset.getString("dessert");
        this.price = rset.getDouble("price");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameMenu() {
        return nameMenu;
    }

    public void setNameMenu(String nameMenu) {
        this.nameMenu = nameMenu;
    }

    public String getStarter() {
        return starter;
    }

    public void setStarter(String starter) {
        this.starter = starter;
    }

    public String getMainCours() {
        return mainCours;
    }

    public void setMainCours(String mainCours) {
        this.mainCours = mainCours;
    }

    public String getDessert() {
        return dessert;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
