package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingSpa {

    private Integer id;
    private Date dateBook;
    private Integer idEmployee;

    public BookingSpa(){}
    public BookingSpa(Integer idB, Date dateBookB, Integer idEmployeeB){
        this.id = idB;
        this.dateBook = dateBookB;
        this.idEmployee = idEmployeeB;
    }
    public BookingSpa(Date dateBookB, Integer idEmployeeB){
        this.dateBook = dateBookB;
        this.idEmployee = idEmployeeB;
    }
    public BookingSpa(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.dateBook = rset.getDate("date_book");
        this.idEmployee = rset.getInt("id_employee");
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateBook() {
        return dateBook;
    }

    public void setDateBook(Date dateBook) {
        this.dateBook = dateBook;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }
}
