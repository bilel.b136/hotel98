package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class BookingTableSet {
    private Integer id;
    private Date dateBook;
    private String service;
    private int idEmployee;

    public BookingTableSet(){}
    public BookingTableSet(Integer idB, Date dateB, String serviceB, Integer idEmployeeB){
        this.id = idB;
        this.dateBook = dateB;
        this.service = serviceB;
        this.idEmployee = idEmployeeB;
    }
    public BookingTableSet(Date dateB, String serviceB, Integer idEmployeeB){
        this.dateBook = dateB;
        this.service = serviceB;
        this.idEmployee = idEmployeeB;
    }
    public BookingTableSet(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.dateBook = rset.getDate("date_book");
        this.service = rset.getString("service");
        this.idEmployee = rset.getInt("id_employee");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateBook() {
        return dateBook;
    }

    public void setDateBook(Date dateBook) {
        this.dateBook = dateBook;
    }

    public String isService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }
}
