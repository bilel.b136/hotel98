package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Agency {

    private Integer id;
    private String nameAgency;
    private boolean typeAgency;

    public Agency(){}
    public Agency(Integer idA, String nameAgencyA, boolean typeAgencyA){
        this.id = idA;
        this.nameAgency = nameAgencyA;
        this.typeAgency = typeAgencyA;
    }
    public Agency(String nameAgencyA, boolean typeAgencyA){
        this.nameAgency = nameAgencyA;
        this.typeAgency = typeAgencyA;
    }

    public Agency(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.nameAgency = rset.getString("name_agency");
        this.typeAgency = rset.getBoolean("type_agency");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameAgency() {
        return nameAgency;
    }

    public void setNameAgency(String nameAgency) {
        this.nameAgency = nameAgency;
    }

    public boolean isTypeAgency() {
        return typeAgency;
    }

    public void setTypeAgency(boolean typeAgency) {
        this.typeAgency = typeAgency;
    }
}
