package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PourUn {

    private Integer id;
    private Integer idBookingTableSet;

    public PourUn(Integer idP, Integer idBookingTableSetP){
        this.id = idP;
        this.idBookingTableSet = idBookingTableSetP;
    }
    public PourUn(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.idBookingTableSet = rset.getInt("id_booking_table_set");
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdBookingTableSet() {
        return idBookingTableSet;
    }

    public void setIdBookingTableSet(Integer idBookingTableSet) {
        this.idBookingTableSet = idBookingTableSet;
    }
}
