package com.entities;

import org.apache.commons.codec.digest.DigestUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Employee {

    private Integer id;
    private String nom;
    private String pnom;
    private String adresse;
    private String login;
    private String password;
    private Integer id_job;

    public Employee(){}
    public Employee(Integer Eid, String Enom, String Epnom, String Eadresse, String Elogin, String Epassword, Integer id_jobE){
        this.id = Eid;
        this.nom = Enom;
        this.pnom = Epnom;
        this.adresse = Eadresse;
        this.login = Elogin;
        this.password = Epassword;
        this.id_job = id_jobE;
    }
    public Employee(String Enom, String Epnom, String Eadresse, String Elogin, String Epassword, Integer id_jobE){
        this.nom = Enom;
        this.pnom = Epnom;
        this.adresse = Eadresse;
        this.login = Elogin;
        this.password = Epassword;
        this.id_job = id_jobE;
    }
    public Employee(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.nom = rset.getString("nom_emp");
        this.pnom = rset.getString("pnom_emp");
        this.adresse = rset.getString("adresse");
        this.login = rset.getString("login");
        this.password = rset.getString("password");
        this.id_job = rset.getInt("id_job");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPnom() {
        return pnom;
    }

    public void setPnom(String pnom) {
        this.pnom = pnom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = DigestUtils.sha256Hex(password);
    }

    public Integer getId_job() {
        return id_job;
    }

    public void setId_job(Integer id_job) {
        this.id_job = id_job;
    }
}