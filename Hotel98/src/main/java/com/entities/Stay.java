package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Stay {

    private Integer id;
    private Date dateStart;
    private Date dateEnd;
    private String stayComment;
    private String state;
    private Integer idAgency;
    private Integer idEmployee;

    public Stay(){}
    public Stay(Integer idS, Date dateStartS, Date dateEndS, String stayCommentS, String stateS, Integer idAgencyS, Integer idEmployeeS){
        this.id = idS;
        this.dateStart = dateStartS;
        this.dateEnd = dateEndS;
        this.stayComment = stayCommentS;
        this.state = stateS;
        this.idAgency = idAgencyS;
        this.idEmployee = idEmployeeS;
    }
    public Stay(Date dateStartS, Date dateEndS, String stayCommentS, String stateS, Integer idAgencyS, Integer idEmployeeS){
        this.dateStart = dateStartS;
        this.dateEnd = dateEndS;
        this.stayComment = stayCommentS;
        this.state = stateS;
        this.idAgency = idAgencyS;
        this.idEmployee = idEmployeeS;
    }
    public Stay(ResultSet rest) throws SQLException {
        this.id = rest.getInt("id");
        this.dateStart = rest.getDate("date_start");
        this.dateEnd = rest.getDate("date_end");
        this.stayComment = rest.getString("stay_comment");
        this.state = rest.getString("state");
        this.idAgency = rest.getInt("id_agency");
        this.idEmployee = rest.getInt("id_employee");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getStayComment() {
        return stayComment;
    }

    public void setStayComment(String stayComment) {
        this.stayComment = stayComment;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getIdAgency() {
        return idAgency;
    }

    public void setIdAgency(Integer idAgency) {
        this.idAgency = idAgency;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }
}
