package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookingRoom {

    private Integer id;
    private Integer id_room;

    public BookingRoom(){}
    public BookingRoom(Integer idB, Integer idRoomB){
        this.id = idB;
        this.id_room = idRoomB;
    }
    public BookingRoom(Integer idRoomB){
        this.id_room = idRoomB;
    }
    public BookingRoom(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.id_room = rset.getInt("id_room");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setId_room(Integer id_room) {
        this.id_room = id_room;
    }

    public Integer getId_room() {
        return id_room;
    }
}
