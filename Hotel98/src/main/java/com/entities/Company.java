package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Company {

    private Integer id;
    private String nameComp;
    private String siret;

    public Company(){}
    public Company(Integer idC, String nameCompC, String siretC){
        this.id = idC;
        this.nameComp = nameCompC;
        this.siret = siretC;
    }
    public Company(String nameCompC, String siretC){
        this.nameComp = nameCompC;
        this.siret = siretC;
    }
    public Company(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.nameComp = rset.getString("name");
        this.siret = rset.getString("siret");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameComp() {
        return nameComp;
    }

    public void setNameComp(String nameComp) {
        this.nameComp = nameComp;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }
}




