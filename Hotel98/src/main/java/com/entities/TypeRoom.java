package com.entities;

import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TypeRoom {

    private Integer id;
    private String name;

    public TypeRoom(){}

    public TypeRoom(Integer idT, String nameT){
        this.id = idT;
        this.name = nameT;
    }
    public TypeRoom(String nameT){
        this.name = nameT;
    }
    public TypeRoom(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.name = rset.getString("name");
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
