package com.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Salary {

    private Integer id;
    private Double salary;
    private Date month;
    private Integer idEmployee;

    public Salary(){}
    public Salary(Integer idS, Double salaryS, Date monthS, Integer idEmployeeS){
        this.id = idS;
        this.salary = salaryS;
        this.month = monthS;
        this.idEmployee = idEmployeeS;
    }
    public Salary(Double salaryS, Date monthS, Integer idEmployeeS){
        this.salary = salaryS;
        this.month = monthS;
        this.idEmployee = idEmployeeS;
    }
    public Salary(ResultSet rset) throws SQLException {
        this.id = rset.getInt("id");
        this.salary = rset.getDouble("salary");
        this.month = rset.getDate("month");
        this.idEmployee = rset.getInt("id_employee");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }
}
