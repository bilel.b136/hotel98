package com.data.database;

import com.entities.Agency;
import com.entities.Employee;
import com.entities.Stock;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class Query {

    public static Statement stmt(){
        Connection con;
        Statement stmt = null;
        {
            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Hostel98", "root", "");
                stmt = con.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return stmt;
    }

    //*********************************************************************************************************
    //Agency
    //*********************************************************************************************************
    public static final String findAllAgencie = "SELECT * FROM `agency`;";
    public static final String addAgency(Agency agence) {
        String name_agency = agence.getNameAgency();
        Boolean type_agency = agence.isTypeAgency();
        return "INSERT INTO agency (name_agency, type_agency) values ('" + name_agency + "', '" + type_agency + "');";
    }
    public static final String updateAgency(Agency agence) {
        String name_agency = agence.getNameAgency();
        Boolean type_agency = agence.isTypeAgency();
        return "UPDATE agency SET name_agency = '" + name_agency + "', type_agency = '" + type_agency + "';";
    }
    public static final String deleteAgencyById(String id) {
        return "DELETE FROM agency WHERE id = '" + id + "';";
    }

    //*********************************************************************************************************
    //BOOKING ROOM
    //*********************************************************************************************************
    public static final String findAllBookingRoom = "Select * from booking_room;";

    //*********************************************************************************************************
    //BOOKING SPA
    //*********************************************************************************************************
    public static final String findAllBookingSpa = "Select * from booking_spa;";

    //*********************************************************************************************************
    //BOOKING TABLE SET
    //*********************************************************************************************************
    public static final String findAllBookingTs = "Select * from booking_table_set;";


    //*********************************************************************************************************
    //BOOKING UNEXEPECTED
    //*********************************************************************************************************
    public static final String findAllBookingUnexpected = "Select * from booking_unexpected;";

    //*********************************************************************************************************
    //COMPANY
    //*********************************************************************************************************
    public static final String findCompanies = "SELECT * FROM `company`;";

    //*********************************************************************************************************
    //COST
    //*********************************************************************************************************
    public static final String findAverageCost = "SELECT AVG(price) FROM `cost`;";


    //*********************************************************************************************************
    //CREATE TICKET
    //*********************************************************************************************************


    //*********************************************************************************************************
    //CUSTOMER
    //*********************************************************************************************************
    public static final String findAllCustomers = "SELECT * FROM `customer`;";
    public static final String findCustomerById(Integer id){ return  "SELECT * FROM `customer` where id="+ id +";";}

    //*********************************************************************************************************
    //CUSTOMER_PRO
    //*********************************************************************************************************
    public static final String findAllCustomersPro = "SELECT * FROM `customer_pro`;";
    public static final String findCustomerProById(Integer id){ return  "SELECT * FROM `customer_pro` where id="+ id +";";}

    //*********************************************************************************************************
    //EMPLOYEE
    //*********************************************************************************************************
    public static final String findAllEmployees = "SELECT * FROM `employee`;";
    public static final String findEmployeeById(Integer id){ return "SELECT * FROM `employee` where id=' + id + ' ;";}
    public static String login(String log, String password){return "SELECT * FROM `employee` where login =\"" + log + "\" and password = \"" + password + "\";";}
    public static final String updateEmployee(Employee currentEmployee){
        return "UPDATE employee SET nom_emp = "+ currentEmployee.getNom() + ", pnom_emp = "+ currentEmployee.getPnom() + ", adresse = "+ currentEmployee.getAdresse() +", login = "+ currentEmployee.getLogin()+", password = "+currentEmployee.getPassword()+";";
    }

    //agency
    public static final String findAllAgencie = "SELECT * FROM `agency`;";

    //booking_room
    public static final String findAllBookingRoom = "Select * from booking_room;";

    //booking_spa
    public static final String findBookingSpa = "SELECT * FROM `booking_spa`;";
    public static final String findAllBookingSpa = "Select * from booking_spa;";

    //booking_table_set
    public static final String findBookingTableSet = "SELECT * FROM `booking_table_set`;";

    //booking_unexpected
    public static final String findBookingUnexpected = "SELECT * FROM `booking_unexpected`;";


    //*********************************************************************************************************
    //OFF
    //*********************************************************************************************************
    public static final String findAllRoomOff = "SELECT * FROM `off`;";

    //cost
    public static final String findCosts = "SELECT * FROM `cost`;";

    //create_ticket


    //customer
    public static final String findCustomers = "SELECT * FROM `customer`;";

    //customer_pro
    public static final String findCustomerPros = "SELECT * FROM `customer_pro`;";

    //date
    public static final String findDates = "SELECT * FROM `date`;";

    //ext_spa
    public static final String findExtSpa = "SELECT * FROM `ext_spa`;";


    //ext_ts
    public static final String findExt_ts = "SELECT * FROM `ext_ts`;";


    //facture
    public static final String findFactures = "SELECT * FROM `facture`;";


    //facturer


    //int_spa
    public static final String findIntSpa = "SELECT * FROM `int_spa`;";

    //int_ts
    public static final String findIntTs = "SELECT * FROM `int_ts`;";

    //job
    public static final String findJobById(Integer id){
        return "SELECT * from `job` where id = " + id +";";
    }

    //menu
    public static final String findMenus = "SELECT * FROM `menu`;";


    //menu_day
    public static final String findMenuDay = "SELECT * FROM `menu_day`;";

    //night
    public static final String findNights = "SELECT * FROM `night`;";

    //off
    public static final String findOff = "SELECT * FROM `off`;";

    //pour_des


    //pour_un


    //recipient_ticket


    //room
    public static final String findRooms = "SELECT * FROM `room`;";

    //salary
    public static final String findSalaries = "SELECT * FROM `salary`;";

    //service
    public static final String findServices = "SELECT * FROM `service`;";
    public static String findServiceById(int id) {
        return "SELECT * from `service` where id = " + id +";";
    }

    //stay
    public static final String findStays = "SELECT * FROM `stay`;";

    //stock
    public static final String findAllStock = "SELECT * FROM `stock`;";
    public static final String countAllStock = "SELECT COUNT(*) as Count FROM `stock`;";


    //ticket
    public static final String findTickets = "SELECT * FROM `ticket`;";
    public static final String findTicketforEmployee(int id){ return "SELECT * FROM `ticket` INNER JOIN recipient_ticket ON recipient_ticket.id WHERE recipient_ticket.id = " +id +";";}
    public static final String findTicketfromEmployee(int id){ return "SELECT * FROM `ticket` INNER JOIN create_ticket ON create_ticket.id WHERE create_ticket.id_employee = " +id +";";}

    //*********************************************************************************************************
    //TYPE ROOM
    //*********************************************************************************************************
    public static final String findTypeRooms = "SELECT * FROM `type_room`;";
    public static final String findTypeRoomById(Integer id){return "SELECT * FROM `type_room where " + id + ";";}

    //*********************************************************************************************************
    //UNIT
    //*********************************************************************************************************
    public static final String findUnits = "SELECT * FROM `unit`;";
    public static final String findByUnit(Integer id){ return "SELECT * FROM `unit` where id=' + id + '";}
}
