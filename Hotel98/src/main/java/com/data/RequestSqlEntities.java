package com.data;


import com.entities.*;
import com.data.database.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RequestSqlEntities {

    //*********************************************************************************************************
    //Agency
    //*********************************************************************************************************

    public List<Agency> findAgencies() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findAllAgencie);
        List<Agency> agencyList = new ArrayList<>();
        try {
            while (rset.next()) {
                agencyList.add(new Agency(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return agencyList;
    }

    public static boolean addAgency(Agency agence) {
        boolean result = false;
        try {
            Query.stmt().executeQuery(Query.addAgency(agence));
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean updateAgency(Agency agence) {
        boolean result = false;
        try {
            Query.stmt().executeQuery(Query.updateAgency(agence));
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteAgencyById(String id) {
        boolean result = false;
        try {
            Query.stmt().executeQuery(Query.deleteAgencyById(id));
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    //*********************************************************************************************************
    //BOOKING ROOM
    //*********************************************************************************************************

    public List<BookingRoom> findAllBookingRoom() throws SQLException {

        ResultSet rset = Query.stmt().executeQuery(Query.findAllBookingRoom);
        List<BookingRoom> bookingRoomList = new ArrayList<BookingRoom>();
        try {
            while (rset.next()) {
                bookingRoomList.add(new BookingRoom(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return bookingRoomList;
    }

    //*********************************************************************************************************
    //BOOKING SPA
    //*********************************************************************************************************

    public List<BookingSpa> findAllBookingSpa() throws SQLException {

        ResultSet rset = Query.stmt().executeQuery(Query.findAllBookingSpa);
        List<BookingSpa> bookingRoomList = new ArrayList<BookingSpa>();
        try {
            while (rset.next()) {
                bookingRoomList.add(new BookingSpa(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return bookingRoomList;
    }

    //*********************************************************************************************************
    //BOOKING TABLE SET
    //*********************************************************************************************************


    //*********************************************************************************************************
    //BOOKING UNEXEPECTED
    //*********************************************************************************************************


    //*********************************************************************************************************
    //COMPANY
    //*********************************************************************************************************

    public List<Company> findCompanies() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findCompanies);
        List<Company> companyList = new ArrayList<>();
        try {
            while (rset.next()) {
                companyList.add(new Company(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return companyList;
    }

    //*********************************************************************************************************
    //COST
    //*********************************************************************************************************


    //*********************************************************************************************************
    //CREATE TICKET
    //*********************************************************************************************************


    //*********************************************************************************************************
    //CUSTOMER
    //*********************************************************************************************************


    //*********************************************************************************************************
    //CUSTOMER_PRO
    //*********************************************************************************************************


    //*********************************************************************************************************
    //EMPLOYEE
    //*********************************************************************************************************

    public Employee getEmployee(Integer id_employee) throws SQLException {
        Employee employee = null;

        ResultSet rsetJob = Query.stmt().executeQuery(Query.findJobById(id_employee));
        if (rsetJob.next()) {
            employee = new Employee(rsetJob.getInt("id"), rsetJob.getString("nom_emp"),rsetJob.getString("pnom_emp"),rsetJob.getString("adresse"),rsetJob.getString("login"), rsetJob.getString("password"), rsetJob.getInt("id_job"));

        }
        return employee;
    }
    public static Employee findEmployeeById(Integer id) throws SQLException {
        Employee employee = new Employee();
        ResultSet rset = Query.stmt().executeQuery(Query.findEmployeeById(id));
        if (rset.next()){
            employee = new Employee(rset);
        }
        return  employee;
    }

    public List<Employee> findAllEmployees() throws SQLException {

        ResultSet rset = Query.stmt().executeQuery(Query.findAllEmployees);
        List<Employee> employeeList = new ArrayList<Employee>();
        try {
            while (rset.next()) {
                employeeList.add(new Employee(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }
    public static Employee login(String log, String password) {
        ResultSet rset = null;
        Employee currentEmployee = null;
        try{
            rset = Query.stmt().executeQuery(Query.login(log,password));
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        try {
            if (rset.next()) {
                currentEmployee = new Employee(rset);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return currentEmployee;
    }
    public void updateEmployee(Employee currentEmployee) throws SQLException {
        Query.stmt().executeUpdate(Query.updateEmployee(currentEmployee));
    }

    //*********************************************************************************************************
    //EXT SPA
    //*********************************************************************************************************

    public List<ExtSpa> findExtSpa() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findExtSpa);
        List<ExtSpa> extSpaList = new ArrayList<>();
        try {
            while (rset.next()) {
                extSpaList.add(new ExtSpa(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return extSpaList;
    }
    //*********************************************************************************************************
    //EXT TS
    //*********************************************************************************************************
    public List<ExtTs> findExt_ts() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findExt_ts);
        List<ExtTs> ext_tsList = new ArrayList<>();
        try {
            while (rset.next()) {
                ext_tsList.add(new ExtTs(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ext_tsList;
    }
    //*********************************************************************************************************
    //FACTURE
    //*********************************************************************************************************


    public List<Facture> findFactures() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findFactures);
        List<Facture> factureList = new ArrayList<>();
        try {
            while (rset.next()) {
                factureList.add(new Facture(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return factureList;
    }

    //*********************************************************************************************************
    //FACTURER
    //*********************************************************************************************************


    //*********************************************************************************************************
    //INT SPA
    //*********************************************************************************************************


    //*********************************************************************************************************
    //INT TS
    //*********************************************************************************************************


    //*********************************************************************************************************
    //JOB
    //*********************************************************************************************************

    public static Job getJobById(Integer id_job) throws SQLException {
        Job job = null;
        Service service = null;

        ResultSet rsetJob = Query.stmt().executeQuery(Query.findJobById(id_job));
        if (rsetJob.next()) {
            ResultSet rsetService = Query.stmt().executeQuery(Query.findServiceById(rsetJob.getInt("id_service")));
            job = new Job(rsetJob.getInt("id"), rsetJob.getString("name_job"), rsetJob.getInt("id_service"));
        }
        return job;
    }

    //*********************************************************************************************************
    //MENU
    //*********************************************************************************************************
    public List<Menu> findMenus() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findAllMenus);
        List<Menu> menuList = new ArrayList<>();
        try {
            while (rset.next()) {
                menuList.add(new Menu(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return menuList;
    }

    //*********************************************************************************************************
    //MENU DAY
    //*********************************************************************************************************


    //*********************************************************************************************************
    //NIGHT
    //*********************************************************************************************************


    //*********************************************************************************************************
    //OFF
    //*********************************************************************************************************


    //*********************************************************************************************************
    //POUR DES
    //*********************************************************************************************************


    //*********************************************************************************************************
    //POUR UN
    //*********************************************************************************************************


    //*********************************************************************************************************
    //RECIPIENT TICKET
    //*********************************************************************************************************


    //*********************************************************************************************************
    //ROOM
    //*********************************************************************************************************


    //*********************************************************************************************************
    //SALARY
    //*********************************************************************************************************


    //*********************************************************************************************************
    //Service
    //*********************************************************************************************************

    public List<Service> findServices() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findServices);
        List<Service> serviceList = new ArrayList<>();
        try {
            while (rset.next()) {
                serviceList.add(new Service(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return serviceList;
    }
    private static Service getServiceById(ResultSet rset) throws SQLException {
        final Integer id = rset.getInt("id");
        final String name = rset.getString("name_service");

        return new Service(id,name);
    }

    //*********************************************************************************************************
    //STAY
    //*********************************************************************************************************


    //*********************************************************************************************************
    //STOCK
    //*********************************************************************************************************
    public static List<Stock> findAllStock() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findAllStock);
        List<Stock> stocksList = new ArrayList<Stock>();
        try {
            while (rset.next()) {
                stocksList.add( new Stock(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return stocksList;
    }

    public static int countAllStock() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.countAllStock);
        int stockComp = 0 ;
        try {
            while (rset.next()) {
                stockComp = rset.getInt("Count");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return stockComp;
    }

    public static boolean addStock(Stock unStock) {
        boolean result = false;
        try {
            Query.stmt().executeQuery(Query.addStock(unStock));
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteStockById(String id) {
        boolean result = false;
        try {
            Query.stmt().executeQuery(Query.deleteStockById(id));
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean updateStock(Stock unStock) {
        boolean result = false;
        try {
            Query.stmt().executeQuery(Query.updateStock(unStock));
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //*********************************************************************************************************
    //TICKET
    //*********************************************************************************************************

    public List<Ticket> getTicketsForEmployee(Integer idEmployee) throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findTicketforEmployee(idEmployee));
        List<Ticket> ticketList = new ArrayList<Ticket>();
        try {
            while (rset.next()) {
                ticketList.add(new Ticket(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ticketList;
    }

    //*********************************************************************************************************
    //TYPE ROOM
    //*********************************************************************************************************

    public List<TypeRoom> findTypeRooms() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findTypeRooms);
        List<TypeRoom> typeRoomList = new ArrayList<>();
        try {
            while (rset.next()) {
                typeRoomList.add(new TypeRoom(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return typeRoomList;
    }
    public TypeRoom getIdRoom(Integer id_room) throws SQLException {
        TypeRoom typeRoom = new TypeRoom();
        ResultSet rsetTypeRoomById = Query.stmt().executeQuery(Query.findTypeRoomById(id_room));
        if (rsetTypeRoomById.next()){
            typeRoom = new TypeRoom(rsetTypeRoomById.getInt("id"), rsetTypeRoomById.getString("name"));
        }
        return typeRoom;
    }

    //*********************************************************************************************************
    //UNIT
    //*********************************************************************************************************

    public static List<Unit> findUnits() throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findUnits);
        List<Unit> unitList = new ArrayList<>();
        try {
            while (rset.next()) {
                unitList.add(new Unit(rset));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitList;
    }
    public static Unit findUnitById(Integer id) throws SQLException {
        ResultSet rset = Query.stmt().executeQuery(Query.findByUnit(id));
        Unit unit = new Unit();
        try {
            if (rset.next()) {
                unit = new Unit(rset);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return unit;
    }
}