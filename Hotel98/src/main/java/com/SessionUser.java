package com;

import com.entities.Employee;

public final class SessionUser {
    private static Employee user;

    public static Employee getUser() {
        return user;
    }

    public static void setUser(Employee user) {
        if (user != null)
            SessionUser.user = user;
    }

    public static void logout(){
        user = null;
    }
}
