package com.controller;

import com.entities.BookingUnexpected;
import com.entities.Stay;
import com.entities.Ticket;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.Date;
import java.util.ResourceBundle;

public class DemandeListItemController extends MainController {
    @FXML
    private AnchorPane rootPane;
    @FXML
    private VBox listMessage;
    @FXML
    private ScrollPane scroll;

    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location, resources);
        try{
            initValue();
        }
        catch(IOException except) {
            System.out.println(except);
        }

    }

    private void initValue() throws IOException{

            scroll.setFitToHeight(true);
            scroll.setFitToWidth(true);
            listMessage.setVgrow(scroll, Priority.ALWAYS);
            for (int ctr = 0; ctr < 10; ctr++){
                FXMLLoader loader = new FXMLLoader();
                listMessage.getChildren().add(loader.load(getClass().getResource("/views/itemDemande.fxml").openStream()));
                ((DemandeItemController)loader.getController()).setDataDemande(new BookingUnexpected(13,"demande","description","en cours",12.00,Date.from(Instant.now()),12));
            }
            listMessage.setSpacing(20);
            scroll.setContent(listMessage);
            rootPane.getChildren().addAll(scroll);
    }
}
