package com.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuItemController extends MainController {
    @FXML
    private Accordion item;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        initValue();
    }

    private void initValue() {
    }

    @FXML
    private void onDeployed(Event event){
        item.getPanes().get(0).setText("menu du jour");
    }
}
