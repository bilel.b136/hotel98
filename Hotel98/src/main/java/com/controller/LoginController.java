package com.controller;


import com.SessionUser;
import com.jfoenix.controls.*;
import com.data.RequestSqlEntities;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * The type Login controller.
 */
public class LoginController extends MainController {

    @FXML
    private JFXTextField fieldUserName;

    @FXML
    private JFXPasswordField fieldPassword;

    @FXML
    private JFXButton btnConnect;

    /**
     * On connect clicked. on redirige vers la bonne page en fonction de l'utilisateur
     *
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    private void onConnectClicked(Event event) throws Exception {

        fieldUserName.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                fieldUserName.validate();
            }
        });
        fieldPassword.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                fieldPassword.validate();
            }
        });
        SessionUser.setUser(RequestSqlEntities.login(fieldUserName.getText(),fieldPassword.getText()));
        if((SessionUser.getUser() != null) && (SessionUser.getUser().getId_job() != null)) {
            switch (RequestSqlEntities.getJobById(SessionUser.getUser().getId_job()).getId()){
                case 3 :    //Directeur d'hotel
                    setCurrentStage(event);
                    changeScene("/views/direction.fxml");
                      this.currentStage.setTitle("Direction");
                    this.currentStage.centerOnScreen();
                    break;
                case 4 :    //Directeur du restaurant
                    setCurrentStage(event);
                    changeScene("/views/restaurant.fxml");
                    this.currentStage.setTitle("Restaurant");
                    this.currentStage.centerOnScreen();
                    break;
                case 5 :    //Directeur de l'hebergement
                    setCurrentStage(event);
                    changeScene("/views/hebergement.fxml");
                    this.currentStage.setTitle("Hébergement");
                    this.currentStage.centerOnScreen();
                    break;
                case 6 :    //Commis de cuisine
                    setCurrentStage(event);
                    changeScene("/views/restaurant.fxml");
                    this.currentStage.setTitle("Reception");
                    this.currentStage.centerOnScreen();
                    break;
                case 7 :    //Chef de reception
                    setCurrentStage(event);
                    changeScene("/views/reception.fxml");
                    this.currentStage.setTitle("Réception");
                    this.currentStage.centerOnScreen();
                    break;
                case 8 :    //Receptionniste
                    setCurrentStage(event);
                    changeScene("/views/reception.fxml");
                    this.currentStage.setTitle("Réception");
                    this.currentStage.centerOnScreen();
                    break;
                case 9 :    //Spa manager
                    setCurrentStage(event);
                    changeScene("/views/spa.fxml");
                    this.currentStage.setTitle("Spa");
                    this.currentStage.centerOnScreen();
                    break;
                case 10 :   //Gouvernance générale
                    setCurrentStage(event);
                    changeScene("/views/menage.fxml");
                    this.currentStage.setTitle("Service d'étage");
                    this.currentStage.centerOnScreen();
                    break;
                case 11 :   //Gouvernante
                    setCurrentStage(event);
                    changeScene("/views/menage.fxml");
                    this.currentStage.setTitle("Service d'étage");
                    this.currentStage.centerOnScreen();
                    break;
                case 12 :   //Femme de chambre
                    setCurrentStage(event);
                    changeScene("/views/menage.fxml");
                    this.currentStage.setTitle("Service d'étage");
                    this.currentStage.centerOnScreen();
                    break;
                case 13 :   //Lingère
                    setCurrentStage(event);
                    changeScene("/views/menage.fxml");
                    this.currentStage.setTitle("Service d'étage");
                    this.currentStage.centerOnScreen();
                    break;
                case 14 :   //Chef de maintenance
                    setCurrentStage(event);
                    changeScene("/views/maintenance.fxml");
                    this.currentStage.setTitle("Maintenance");
                    this.currentStage.centerOnScreen();
                    break;
                case 15 :   //Technicien de maintenance
                    setCurrentStage(event);
                    changeScene("/views/maintenance.fxml");
                    this.currentStage.setTitle("Maintenance");
                    this.currentStage.centerOnScreen();
                    break;
                default :
                    setCurrentStage(event);
                    changeScene("/views/direction.fxml");
                    this.currentStage.setTitle("direction");
                    this.currentStage.centerOnScreen();
                    break;
            }
        }else if (fieldUserName.validate() && fieldPassword.validate()) {
            JFXPopup alert = new JFXPopup();
            JFXDialogLayout layout = new JFXDialogLayout();
            layout.setBody(new Label("Veuillez vérifier vos informations de connexion"));
            alert.setPopupContent(layout);
            alert.show(rootPane, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT);

        }
    }

    @FXML
    private void onKeyPressed(Event event) throws Exception {
        if (((KeyEvent) event).getCode() == KeyCode.ENTER)
            onConnectClicked(event);
    }
}
