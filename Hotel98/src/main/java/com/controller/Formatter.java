package com.controller;

import com.entities.Stock;

public class Formatter {

    private Stock stock;

    public Formatter(Stock stock){
        this.stock = stock;
    }
    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

}
