package com.controller;

import com.entities.BookingUnexpected;
import com.entities.CreateTicket;
import com.entities.RecipientTicket;
import com.jfoenix.controls.JFXTextField;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class DemandeItemController extends MainController {
    @FXML
    private Circle status;

    @FXML
    private Label demandeDate;

    @FXML
    private Label Sujet;

    @FXML
    private Label Objet;

    @FXML
    private JFXTextField prix;

    @FXML
    void repondre(MouseEvent event) {

    }

    @FXML
    void resoudre(MouseEvent event) {

    }

    @FXML
    void transmettre(MouseEvent event) {

    }

    private BookingUnexpected dataDemande;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        initValue();
    }

    private void initValue() {
    }

    @FXML
    private void switcher(Event event){ /* mettre à jour l'etat en base egalement*/
        Circle source = (Circle) event.getSource();
        if (source.getStyleClass().indexOf("active") < 0){
            if(source.getStyleClass().indexOf("desactivate") > -1)
                source.getStyleClass().remove(source.getStyleClass().indexOf("desactivate"));
            source.getStyleClass().add("active");
        }
        else{
            source.getStyleClass().remove(source.getStyleClass().indexOf("active"));
            source.getStyleClass().add("desactivate");
        }
    }

    public BookingUnexpected getDataDemande() {
        return dataDemande;
    }

    public void setDataDemande(BookingUnexpected dataDemande) {
        this.dataDemande = dataDemande;
        this.Objet.setText(dataDemande.getName());
        this.Sujet.setText(dataDemande.getDescription());
    }
}
