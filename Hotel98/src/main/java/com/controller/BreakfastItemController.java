package com.controller;

import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class BreakfastItemController extends MainController {

    @FXML
    private JFXDatePicker datePicker;

    @FXML
    private JFXListView listResa;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        datePicker.setPromptText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.update();

    }

    @FXML
    void update() {
        listResa.getItems().add(new Label("initialiser avec les données de la base"));
    }

}
