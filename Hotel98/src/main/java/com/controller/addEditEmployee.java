package com.controller;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;


public class addEditEmployee extends MainController{
    @FXML
    private JFXTextField name;

    @FXML
    private JFXTextField firstname;

    @FXML
    private JFXComboBox<?> service;

    @FXML
    private JFXComboBox<?> job;

    @FXML
    private JFXTextField mail;

    @FXML
    private JFXTextField login;

    @FXML
    private JFXPasswordField password;

    @FXML
    void validate(MouseEvent event) {

    }

}
