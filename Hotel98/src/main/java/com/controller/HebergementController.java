package com.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Restaurant controller.
 */
public class HebergementController extends MainController {

    @FXML
    protected GridPane componant;

    private AnchorPane anchor;


    @FXML
    protected Text titreItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        try{
            componant.add((AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemLunch.fxml")),1,1);
        }
        catch(IOException excception){
        }
    }
}
