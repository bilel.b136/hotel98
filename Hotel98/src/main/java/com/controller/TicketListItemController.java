package com.controller;

import com.entities.Ticket;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TicketListItemController extends MainController {
    @FXML
    private AnchorPane rootPane;
    @FXML
    private VBox listMessage;

    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location, resources);
        try{
            initValue();
        }
        catch(IOException except) {
            System.out.println(except);
        }

    }

    private void initValue() throws IOException{

            ScrollPane scroll = new ScrollPane();
            scroll.setFitToHeight(true);
            scroll.setFitToWidth(true);
            VBox.setVgrow(scroll, Priority.ALWAYS);
            for (int ctr = 0; ctr < 10; ctr++){
                FXMLLoader loader = new FXMLLoader();
                listMessage.getChildren().add(loader.load(getClass().getResource("/views/itemTicket.fxml").openStream()));
                ((TicketItemController)loader.getController()).setDataTicket(new Ticket(12,"ceci est mon premier message","suuuujet",12));
            }
            listMessage.setSpacing(20);
            scroll.setContent(listMessage);
            rootPane.getChildren().addAll(scroll);
    }
}
