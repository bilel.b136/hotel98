package com.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.*;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;


public class LunchItemController extends MainController{

    @FXML
    private JFXDatePicker datePicker;

    @FXML
    private JFXListView listResa;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        datePicker.setPromptText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.update();

    }

    @FXML
    void update() {
        listResa.getItems().add(new Label("initialiser avec les données de la base"));

    }

}
