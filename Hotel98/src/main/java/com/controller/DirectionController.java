package com.controller;

import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Restaurant controller.
 */
public class DirectionController extends MainController {
    

    @FXML
    private AnchorPane rootPane;
    @FXML
    private JFXHamburger burgerMenu;

    @FXML
    protected GridPane componant;

    @FXML
    protected Text titreItem;

    private AnchorPane anchor;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        try{
            this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemDirecteur.fxml"));
            componant.add(this.anchor, 1, 1);
        }
        catch(IOException excception){
        }
    }

    @FXML
    public void burgerMenuClicked(MouseEvent mouseEvent) {

        JFXListView<Label> list = new JFXListView<>();
        for (int i = 1; i < 5; i++) {
            list.getItems().add(new Label("Item " + i));
        }

        AnchorPane menu = new AnchorPane();
        menu.setPrefHeight(200);
        Label titreMenu = new Label("Acceder aux tableaux de bords");
        titreMenu.getStyleClass().add("menu-titre");
        Pane rect = new Pane(titreMenu);
        rect.getStyleClass().add("menu-header");
        rect.setPrefHeight(100);

        menu.getChildren().addAll(rect, list);
        AnchorPane.setTopAnchor(rect,0.0);
        AnchorPane.setLeftAnchor(rect,0.0);
        AnchorPane.setRightAnchor(rect,0.0);
        AnchorPane.setLeftAnchor(list,0.0);
        AnchorPane.setRightAnchor(list,0.0);
        AnchorPane.setBottomAnchor(list, 0.0);

        JFXPopup popup = new JFXPopup(menu);
        popup.show(rootPane, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT);
    }

    public void onEmployeClicked(MouseEvent mouseEvent) {
        setCurrentStage(mouseEvent);
        this.promptModale("/views/stockModale.fxml","modale employe");
    }
}
