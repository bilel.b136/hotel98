package com.controller;

import com.data.database.Query;
import com.entities.Stock;
import com.entities.Unit;
import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.data.RequestSqlEntities;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.ComboBoxTreeTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Function;

public class ModaleStockController extends MainController{

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        try {
            setupEditableTableView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static final String POSTFIX = " résultats";

    // editable table view
    @FXML
    private JFXTreeTableView<StockFormatter> editableTreeTableView;
    @FXML
    private JFXTreeTableColumn<StockFormatter, String> quantiteEditableColumn;
    @FXML
    private JFXTreeTableColumn<StockFormatter, String> nomProduitEditableColumn;
    @FXML
    private JFXTreeTableColumn<StockFormatter, String> uniteEditableColumn;
    @FXML
    private Text editableTreeTableViewCount;
    @FXML
    private JFXTextField searchField;
    @FXML
    private JFXButton add;
    @FXML
    private JFXButton remove;


    private <T> void setupCellValueFactory(JFXTreeTableColumn<StockFormatter, T> column, Function<StockFormatter, ObservableValue<T>> mapper) {
        column.setCellValueFactory((TreeTableColumn.CellDataFeatures<StockFormatter, T> param) -> {
            if (column.validateValue(param)) {
                return mapper.apply(param.getValue().getValue());
            } else {
                return column.getComputedValue(param);
            }
        });
    }

    public void deleteRow(Event event){
        if((((KeyEvent)event).getCode() == KeyCode.DELETE)){
            try {
                removeData(editableTreeTableView.getSelectionModel().getSelectedItem().getValue());
                editableTreeTableView.getSelectionModel().clearSelection();
            }
            catch(SQLException except){
                System.out.println("failed to remove row");
            }
        }
    }

    private void setupEditableTableView() throws SQLException {
        setupCellValueFactory(nomProduitEditableColumn, StockFormatter::productNameProperty );
        setupCellValueFactory(quantiteEditableColumn, StockFormatter::quantityProperty);

        ObservableList<String> comboList = FXCollections.observableArrayList();
        for (Unit unit : RequestSqlEntities.findUnits()){
            comboList.add(unit.getName());
        }
        uniteEditableColumn.setCellFactory(ComboBoxTreeTableCell.forTreeTableColumn(new DefaultStringConverter(), comboList));

        // add editors
        nomProduitEditableColumn.setCellFactory((TreeTableColumn<StockFormatter, String> param) -> {
            return new GenericEditableTreeTableCell<>(
                    new TextFieldEditorBuilder());
        });
        nomProduitEditableColumn.setOnEditCommit((TreeTableColumn.CellEditEvent<StockFormatter, String> t) -> {
            t.getTreeTableView()
                    .getTreeItem(t.getTreeTablePosition()
                            .getRow())
                    .getValue().productName.set(t.getNewValue());
        });
        quantiteEditableColumn.setCellFactory((TreeTableColumn<StockFormatter, String> param) -> {
            return new GenericEditableTreeTableCell<>(
                    new TextFieldEditorBuilder());
        });
        quantiteEditableColumn.setOnEditCommit((TreeTableColumn.CellEditEvent<StockFormatter, String> t) -> {
            t.getTreeTableView()
                    .getTreeItem(t.getTreeTablePosition()
                            .getRow())
                    .getValue().quantity.set(t.getNewValue());
        });

        final ObservableList<StockFormatter> data = getData();
        editableTreeTableView.setRoot(new RecursiveTreeItem<>(data, RecursiveTreeObject::getChildren));
        editableTreeTableView.setShowRoot(false);
        editableTreeTableView.setEditable(true);
        editableTreeTableViewCount.textProperty()
            .bind(Bindings.createStringBinding(() -> editableTreeTableView.getCurrentItemsCount() + POSTFIX,
            editableTreeTableView.currentItemsCountProperty()));

        searchField.textProperty()
            .addListener(setupSearchField(editableTreeTableView));

        add.setOnMouseClicked((e) -> {
            data.add(new StockFormatter("name","1","1"));
            final IntegerProperty currCountProp = editableTreeTableView.currentItemsCountProperty();
            currCountProp.set(currCountProp.get() + 1);
        });

        remove.setOnMouseClicked((f) -> {
            try{
                removeData(editableTreeTableView.getSelectionModel().getSelectedItem().getValue());
                editableTreeTableView.getSelectionModel().clearSelection();
            }
            catch(SQLException e){
                System.out.println("failed to remove row "+ e.getSQLState());
            }

            final IntegerProperty currCountProp = editableTreeTableView.currentItemsCountProperty();
            currCountProp.set(currCountProp.get() - 1);
        });
}

    private ChangeListener<String> setupSearchField(final JFXTreeTableView<StockFormatter> tableView) {
        return (o, oldVal, newVal) ->
                tableView.setPredicate(stockProp -> {
                    final StockFormatter stock = stockProp.getValue();
                    return stock.productName.get().contains(newVal)
                            || stock.quantity.get().contains(newVal)
                            || stock.unit.get().contains(newVal);
                });
    }

    private ObservableList<StockFormatter> getData() throws SQLException {
        final ObservableList<StockFormatter> data = FXCollections.observableArrayList();
        for (Stock stock : RequestSqlEntities.findAllStock()){
            data.add(new StockFormatter(stock.getName(), stock.getQuantity().toString(), RequestSqlEntities.findUnitById(stock.getIdUnit()).getName()));
        }
        return data;
    }

    private void removeData(StockFormatter data) throws  SQLException{
        /**** enlever la donnée ****/
    }


    /*
     * data class
     */
    static final class StockFormatter extends RecursiveTreeObject<StockFormatter> {
        final StringProperty productName;
        final StringProperty quantity;
        final StringProperty unit;

        StockFormatter(String productName, String quantite, String unit) {
            this.productName = new SimpleStringProperty(productName);
            this.quantity = new SimpleStringProperty(quantite);
            this.unit = new SimpleStringProperty(unit);
        }

        StringProperty productNameProperty() {
            return productName;
        }

        StringProperty quantityProperty() { return quantity; }

        StringProperty unitProperty(){return unit;}

    }
}
