package com.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Restaurant controller.
 */
public class RestaurantController extends MainController {

    @FXML
    protected GridPane componant;

    private AnchorPane anchor;

    @FXML
    protected Text titreItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        try{
            this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemMenu.fxml"));
            componant.add(this.anchor, 1, 1);
        }
        catch(IOException excception){
        }
    }

    /**
     * On stock clicked.
     */
    @FXML
    public void onStockClicked(javafx.event.Event event) {
        setCurrentStage(event);
        this.promptModale("/views/stockModale.fxml","modale stock");
    }

    @FXML
    public void onBreakfastClicked(javafx.event.Event event) throws IOException{
        this.componant.getChildren().removeAll(this.anchor);
        this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemBreakfast.fxml"));
        componant.add(this.anchor, 1, 1);
       titreItem.setText("Petits Dejeuners");
    }

    @FXML
    public void onHomeClicked(Event event) throws IOException{
        this.componant.getChildren().removeAll(this.anchor);
        this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemMenu.fxml"));
        componant.add(this.anchor, 1, 1);
        titreItem.setText("Menus");
    }

    @FXML
    public void onLunchClicked(Event event) throws IOException{
        this.componant.getChildren().removeAll(this.anchor);
        this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemLunch.fxml"));
        componant.add(this.anchor, 1, 1);
        titreItem.setText("Repas");
    }
}
