package com.controller;

import com.entities.CreateTicket;
import com.entities.RecipientTicket;
import com.entities.Ticket;
import com.jfoenix.controls.JFXTextField;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class TicketItemController extends MainController {
    @FXML
    private Circle status;

    @FXML
    private Label demandeDate;

    @FXML
    private Label Sujet;

    @FXML
    private Label Objet;

    @FXML
    private JFXTextField prix;

    @FXML
    void repondre(MouseEvent event) {

    }

    @FXML
    void resoudre(MouseEvent event) {

    }

    @FXML
    void transmettre(MouseEvent event) {

    }

    private Ticket dataTicket;
    private RecipientTicket ticketRecipient;
    private CreateTicket ticketCreator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
    }



    @FXML
    private void switcher(Event event){ /* mettre à jour l'etat en base egalement*/
        Circle source = (Circle) event.getSource();
        if (source.getStyleClass().indexOf("active") < 0){
            if(source.getStyleClass().indexOf("desactivate") > -1)
                source.getStyleClass().remove(source.getStyleClass().indexOf("desactivate"));
            source.getStyleClass().add("active");
        }
        else{
            source.getStyleClass().remove(source.getStyleClass().indexOf("active"));
            source.getStyleClass().add("desactivate");
        }
    }

    public Ticket getDataTicket() {
        return dataTicket;
    }

    public void setDataTicket(Ticket dataTicket) {
        this.dataTicket = dataTicket;
        this.Objet.setText(dataTicket.getObjectTicket());
        this.Sujet.setText(dataTicket.getSubjectTicket());
    }
}
