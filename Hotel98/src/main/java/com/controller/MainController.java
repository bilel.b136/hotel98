package com.controller;

import com.SessionUser;
import com.jfoenix.controls.JFXDecorator;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Main controller.
 */
public class MainController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    /**
     * The Root pane.
     */
    @FXML
    protected AnchorPane rootPane;
    @FXML
    protected Text titreItem;

    /**
     * The Current scene.
     */
    protected Scene currentScene;
    /**
     * The Current stage.
     */
    protected Stage currentStage;

    /**
     * Change scene.
     *
     * @param location the location
     */
    protected void changeScene(String location){
        try{
        this.currentScene = new Scene(FXMLLoader.load(getClass().getResource(location)));
        this.currentStage.setScene(this.currentScene);
        }
        catch(IOException ex){
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "modale", ex);
        }
    }

    /**
     * Prompt modale.
     *
     * @param location the location
     * @param title    the title
     */
    protected void promptModale(String location, String title){
        try {
            Parent parent = FXMLLoader.load(getClass().getResource(location));
            Stage modalStage = new Stage(StageStyle.DECORATED);
            modalStage.initModality(Modality.APPLICATION_MODAL);
            modalStage.setTitle(title);
            modalStage.setScene(new Scene(parent));
            modalStage.getIcons().add(new Image(this.getClass().getResourceAsStream("/img/icon.png")));
            modalStage.show();
        }
        catch(IOException ex){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, "modale", ex);
        }
    }


    /**
     * Show stage.
     */
    public void showStage() {

        currentStage.show();
    }


    /**
     * Set current stage.
     *
     * @param stage the stage
     */
    public void setCurrentStage(Stage stage){
        this.currentStage = stage;
    }


    /**
     * Détermine le stage courant à partir d'un event
     *
     * @param event the event
     */
    protected void setCurrentStage(javafx.event.Event event){
        this.currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    }

    /**
     * On logout clicked. methode générique pour toutes les scenes
     *
     * @param event the event
     */
    @FXML
    protected void onLogoutClicked(javafx.event.Event event){
        setCurrentStage(event);
        changeScene("/views/login.fxml");
        this.currentStage.setTitle("Login");
        this.currentStage.centerOnScreen();
        SessionUser.logout();
    }
}
