package com.controller;

import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type item Directeur controller.
 */
public class itemDirectionController extends MainController {

    @FXML
    private VBox item1;

    @FXML
    private VBox item2;

    @FXML
    private VBox item3;

    @FXML
    private VBox item4;

    @FXML
    private VBox item5;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        item1.setPadding(new Insets(10.0));
        item1.getChildren().add(new Label("Taux journalier de remplissage de l'hotel sur la semaine" + "50%"));
        item1.getChildren().add(new Label("50%"));

        item2.setPadding(new Insets(10.0));
        item2.getChildren().add(new Label("Panier moyen"));
        item2.getChildren().add(new Label("340€"));

        item3.setPadding(new Insets(10.0));
        item3.getChildren().add(new Label("Nombre de petits déjeuners"));
        item3.getChildren().add(new Label("30"));

        item4.setPadding(new Insets(10.0));
        item4.getChildren().add(new Label("Durée moyenne des séjours"));
        item4.getChildren().add(new Label("5 jours"));

        item5.setPadding(new Insets(10.0));
        item5.getChildren().add(new Label("Dépense par service"));

        /* box des différents services */
        HBox boxResto = new HBox();
        boxResto.getStyleClass().add("basicTextField");
        boxResto.setAlignment(Pos.CENTER_LEFT);
        boxResto.setSpacing(10.0);
        boxResto.getChildren().add(new Label("Restaurant"));
        JFXTextField tfResto = new JFXTextField("625€");/* initialiser avec les valeurs en bases */
        tfResto.getStyleClass().add("basicTextField");
        tfResto.setPrefWidth(60.0);
        tfResto.setEditable(false);
        boxResto.getChildren().add(tfResto);

        HBox boxSpa = new HBox();
        boxSpa.getStyleClass().add("basicTextField");
        boxSpa.setAlignment(Pos.CENTER_LEFT);
        boxSpa.setSpacing(10.0);
        boxSpa.getChildren().add(new Label("Spa"));
        JFXTextField tfSpa = new JFXTextField("200€");/* initialiser avec les valeurs en bases */
        tfSpa.getStyleClass().add("basicTextField");
        tfSpa.setPrefWidth(60.0);
        tfSpa.setEditable(false);
        boxSpa.getChildren().add(tfSpa);

        HBox boxMtc = new HBox();
        boxMtc.getStyleClass().add("basicTextField");
        boxMtc.setAlignment(Pos.CENTER_LEFT);
        boxMtc.setSpacing(10.0);
        boxMtc.getChildren().add(new Label("Maintenance"));
        JFXTextField tfMtc = new JFXTextField("200€");/* initialiser avec les valeurs en bases */
        tfMtc.getStyleClass().add("basicTextField");
        tfMtc.setPrefWidth(60.0);
        tfMtc.setEditable(false);
        boxMtc.getChildren().add(tfMtc);

        HBox boxHeb = new HBox();
        boxHeb.getStyleClass().add("basicTextField");
        boxHeb.setAlignment(Pos.CENTER_LEFT);
        boxHeb.setSpacing(10.0);
        boxHeb.getChildren().add(new Label("Hebergement"));
        JFXTextField tfHeb = new JFXTextField("200€");/* initialiser avec les valeurs en bases */
        tfHeb.getStyleClass().add("basicTextField");
        tfHeb.setPrefWidth(60.0);
        tfHeb.setEditable(false);
        boxHeb.getChildren().add(tfHeb);

        HBox boxMen = new HBox();
        boxMen.getStyleClass().add("basicTextField");
        boxMen.setAlignment(Pos.CENTER_LEFT);
        boxMen.setSpacing(10.0);
        boxMen.getChildren().add(new Label("Menage"));
        JFXTextField tfMen = new JFXTextField("200€");/* initialiser avec les valeurs en bases */
        tfMen.getStyleClass().add("basicTextField");
        tfMen.setPrefWidth(60.0);
        tfMen.setEditable(false);
        boxMen.getChildren().add(tfMen);

        HBox boxRec = new HBox();
        boxRec.getStyleClass().add("basicTextField");
        boxRec.setAlignment(Pos.CENTER_LEFT);
        boxRec.setSpacing(10.0);
        boxRec.getChildren().add(new Label("Reception"));
        JFXTextField tfRec = new JFXTextField("200€");/* initialiser avec les valeurs en bases */
        tfRec.getStyleClass().add("basicTextField");
        tfRec.setPrefWidth(60.0);
        tfRec.setEditable(false);
        boxRec.getChildren().add(tfRec);



        /* ajout box dans l'item */
        item5.setSpacing(10.00);
        item5.getChildren().addAll(boxResto ,boxSpa, boxMtc, boxHeb, boxMen, boxRec);
    }
}
