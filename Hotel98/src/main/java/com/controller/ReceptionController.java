package com.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Restaurant controller.
 */
public class ReceptionController extends MainController {

    @FXML
    private GridPane componant;

    private AnchorPane anchor;

    /**
     * componant
     */
    @FXML
    protected BorderPane componantPane;

    @FXML
    protected Text titreItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        try{
            this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemListeDemande.fxml"));
            componant.add(this.anchor, 1, 1);
        }
        catch(IOException excception){
        }
    }

    /**
     * On stock clicked.
     */
    @FXML
    public void onReservationClicked(Event event) {
        setCurrentStage(event);
        this.promptModale("/views/stockModale.fxml","modale stock");
    }

    @FXML
    public void onSpaClicked(Event event) throws IOException{
        this.componant.getChildren().removeAll(this.anchor);
        this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemBreakfast.fxml"));
        componant.add(this.anchor, 1, 1);
       titreItem.setText("Spa");
    }

    @FXML
    public void onHomeClicked(Event event) throws IOException{
        this.componant.getChildren().removeAll(this.anchor);
        this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemListeDemande.fxml"));
        componant.add(this.anchor, 1, 1);
        titreItem.setText("Liste des demandes client");
    }

    @FXML
    public void onLunchClicked(Event event) throws IOException{
        this.componant.getChildren().removeAll(this.anchor);
        this.anchor = (AnchorPane) FXMLLoader.load(getClass().getResource("/views/itemLunch.fxml"));
        componant.add(this.anchor, 1, 1);
        titreItem.setText("Repas");
    }
}
