-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 11 juin 2018 à 09:22
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hotel98`
--

-- --------------------------------------------------------

--
-- Structure de la table `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `name_agency` varchar(25) NOT NULL,
  `type_agency` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `booking_room`
--

CREATE TABLE `booking_room` (
  `id` int(11) NOT NULL,
  `id_type_room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `booking_spa`
--

CREATE TABLE `booking_spa` (
  `id` int(11) NOT NULL,
  `date_book` datetime NOT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `booking_table_set`
--

CREATE TABLE `booking_table_set` (
  `id` int(11) NOT NULL,
  `date_book` date NOT NULL,
  `service` varchar(25) NOT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `booking_unexpected`
--

CREATE TABLE `booking_unexpected` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` varchar(25) NOT NULL,
  `state` varchar(25) NOT NULL,
  `price` double DEFAULT NULL,
  `created` date NOT NULL,
  `id_stay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `breakfast`
--

CREATE TABLE `breakfast` (
  `id` int(11) NOT NULL,
  `libel` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name_comp` varchar(25) NOT NULL,
  `siret` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cost`
--

CREATE TABLE `cost` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `price` double NOT NULL,
  `id_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `create_ticket`
--

CREATE TABLE `create_ticket` (
  `id` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `phone_number` tinyint(4) NOT NULL,
  `postal_code` tinyint(4) NOT NULL,
  `address` varchar(25) NOT NULL,
  `date_book` date NOT NULL,
  `id_stay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `customer_pro`
--

CREATE TABLE `customer_pro` (
  `id` int(11) NOT NULL,
  `id_company` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `date`
--

CREATE TABLE `date` (
  `cle` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `nom_emp` varchar(25) NOT NULL,
  `pnom_emp` varchar(25) NOT NULL,
  `adresse` varchar(25) NOT NULL,
  `login` varchar(25) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL,
  `id_job` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employee`
--

INSERT INTO `employee` (`id`, `nom_emp`, `pnom_emp`, `adresse`, `login`, `password`, `id_job`) VALUES
(0, 'me demande pas pk', 'lol', 'ad', 'lol', 'lol', 2),
(1, 'bili', 'boy', '58 ujk', 'baba', 'baba', 2);

-- --------------------------------------------------------

--
-- Structure de la table `ext_spa`
--

CREATE TABLE `ext_spa` (
  `name_book` varchar(25) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ext_ts`
--

CREATE TABLE `ext_ts` (
  `name_book` varchar(25) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE `facture` (
  `id` int(11) NOT NULL,
  `price` double NOT NULL,
  `date_facture` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `facturer`
--

CREATE TABLE `facturer` (
  `id` int(11) NOT NULL,
  `id_facture` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `int_spa`
--

CREATE TABLE `int_spa` (
  `id` int(11) NOT NULL,
  `id_stay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `int_ts`
--

CREATE TABLE `int_ts` (
  `id` int(11) NOT NULL,
  `id_stay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `name_job` varchar(25) NOT NULL,
  `id_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `job`
--

INSERT INTO `job` (`id`, `name_job`, `id_service`) VALUES
(2, 'boss', 1);

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name_menu` varchar(25) NOT NULL,
  `starter` varchar(25) NOT NULL,
  `main_cours` varchar(25) NOT NULL,
  `dessert` varchar(25) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `menu_day`
--

CREATE TABLE `menu_day` (
  `id` int(11) NOT NULL,
  `name_menu` varchar(25) NOT NULL,
  `starter` varchar(25) NOT NULL,
  `dessert` varchar(25) NOT NULL,
  `main_cours` varchar(25) DEFAULT NULL,
  `cle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `night`
--

CREATE TABLE `night` (
  `id` int(11) NOT NULL,
  `date_night` date NOT NULL,
  `id_room` int(11) NOT NULL,
  `state` tinyint(1) DEFAULT NULL,
  `id_breakfast` int(11) NOT NULL,
  `id_stay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `off`
--

CREATE TABLE `off` (
  `id` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `description` varchar(25) DEFAULT NULL,
  `id_room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pour_des`
--

CREATE TABLE `pour_des` (
  `id` int(11) NOT NULL,
  `id_booking_table_set` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pour_un`
--

CREATE TABLE `pour_un` (
  `id` int(11) NOT NULL,
  `id_booking_table_set` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `recipient_ticket`
--

CREATE TABLE `recipient_ticket` (
  `id` int(11) NOT NULL,
  `id_ticket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `num_room` int(11) NOT NULL,
  `price` float NOT NULL,
  `id_type_room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `salary` double DEFAULT NULL,
  `month` date NOT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name_service` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `service`
--

INSERT INTO `service` (`id`, `name_service`) VALUES
(1, 'lol');

-- --------------------------------------------------------

--
-- Structure de la table `stay`
--

CREATE TABLE `stay` (
  `id` int(11) NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `beneficiary` tinyint(4) NOT NULL,
  `stay_comment` text,
  `state` tinyint(1) NOT NULL,
  `id_agency` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `id_unit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `object_ticket` varchar(25) DEFAULT NULL,
  `subject_ticket` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type_room`
--

CREATE TABLE `type_room` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `name` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `booking_room`
--
ALTER TABLE `booking_room`
  ADD PRIMARY KEY (`id`,`id_type_room`),
  ADD KEY `FK_booking_room_id_type_room` (`id_type_room`);

--
-- Index pour la table `booking_spa`
--
ALTER TABLE `booking_spa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_booking_spa_id_employee` (`id_employee`);

--
-- Index pour la table `booking_table_set`
--
ALTER TABLE `booking_table_set`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_booking_table_set_id_employee` (`id_employee`);

--
-- Index pour la table `booking_unexpected`
--
ALTER TABLE `booking_unexpected`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_booking_unexpected_id_stay` (`id_stay`);

--
-- Index pour la table `breakfast`
--
ALTER TABLE `breakfast`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_cost_id_service` (`id_service`);

--
-- Index pour la table `create_ticket`
--
ALTER TABLE `create_ticket`
  ADD PRIMARY KEY (`id`,`id_employee`),
  ADD KEY `FK_create_ticket_id_employee` (`id_employee`);

--
-- Index pour la table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_customer_id_stay` (`id_stay`);

--
-- Index pour la table `customer_pro`
--
ALTER TABLE `customer_pro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_customer_pro_id_company` (`id_company`);

--
-- Index pour la table `date`
--
ALTER TABLE `date`
  ADD PRIMARY KEY (`cle`),
  ADD KEY `FK_date_id` (`id`);

--
-- Index pour la table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_employee_id_job` (`id_job`);

--
-- Index pour la table `ext_spa`
--
ALTER TABLE `ext_spa`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ext_ts`
--
ALTER TABLE `ext_ts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `facture`
--
ALTER TABLE `facture`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `facturer`
--
ALTER TABLE `facturer`
  ADD PRIMARY KEY (`id`,`id_facture`),
  ADD KEY `FK_facturer_id_facture` (`id_facture`);

--
-- Index pour la table `int_spa`
--
ALTER TABLE `int_spa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_int_spa_id_stay` (`id_stay`);

--
-- Index pour la table `int_ts`
--
ALTER TABLE `int_ts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_int_ts_id_stay` (`id_stay`);

--
-- Index pour la table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_job_id_service` (`id_service`);

--
-- Index pour la table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `menu_day`
--
ALTER TABLE `menu_day`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_menu_day_cle` (`cle`);

--
-- Index pour la table `night`
--
ALTER TABLE `night`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_night_id_room` (`id_room`),
  ADD KEY `FK_night_id_breakfast` (`id_breakfast`),
  ADD KEY `FK_night_id_stay` (`id_stay`);

--
-- Index pour la table `off`
--
ALTER TABLE `off`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_off_id_room` (`id_room`);

--
-- Index pour la table `pour_des`
--
ALTER TABLE `pour_des`
  ADD PRIMARY KEY (`id`,`id_booking_table_set`),
  ADD KEY `FK_pour_des_id_booking_table_set` (`id_booking_table_set`);

--
-- Index pour la table `pour_un`
--
ALTER TABLE `pour_un`
  ADD PRIMARY KEY (`id`,`id_booking_table_set`),
  ADD KEY `FK_pour_un_id_booking_table_set` (`id_booking_table_set`);

--
-- Index pour la table `recipient_ticket`
--
ALTER TABLE `recipient_ticket`
  ADD PRIMARY KEY (`id`,`id_ticket`),
  ADD KEY `FK_recipient_ticket_id_ticket` (`id_ticket`);

--
-- Index pour la table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_room_id_type_room` (`id_type_room`);

--
-- Index pour la table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_salary_id_employee` (`id_employee`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `stay`
--
ALTER TABLE `stay`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_stay_id_agency` (`id_agency`),
  ADD KEY `FK_stay_id_employee` (`id_employee`);

--
-- Index pour la table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_stock_id_employee` (`id_employee`),
  ADD KEY `FK_stock_id_unit` (`id_unit`);

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_room`
--
ALTER TABLE `type_room`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `booking_spa`
--
ALTER TABLE `booking_spa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `booking_table_set`
--
ALTER TABLE `booking_table_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `booking_unexpected`
--
ALTER TABLE `booking_unexpected`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `breakfast`
--
ALTER TABLE `breakfast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cost`
--
ALTER TABLE `cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `date`
--
ALTER TABLE `date`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `facture`
--
ALTER TABLE `facture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `menu_day`
--
ALTER TABLE `menu_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `night`
--
ALTER TABLE `night`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `off`
--
ALTER TABLE `off`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `stay`
--
ALTER TABLE `stay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `booking_room`
--
ALTER TABLE `booking_room`
  ADD CONSTRAINT `FK_booking_room_id` FOREIGN KEY (`id`) REFERENCES `stay` (`id`),
  ADD CONSTRAINT `FK_booking_room_id_type_room` FOREIGN KEY (`id_type_room`) REFERENCES `type_room` (`id`);

--
-- Contraintes pour la table `booking_spa`
--
ALTER TABLE `booking_spa`
  ADD CONSTRAINT `FK_booking_spa_id_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`);

--
-- Contraintes pour la table `booking_table_set`
--
ALTER TABLE `booking_table_set`
  ADD CONSTRAINT `FK_booking_table_set_id_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`);

--
-- Contraintes pour la table `booking_unexpected`
--
ALTER TABLE `booking_unexpected`
  ADD CONSTRAINT `FK_booking_unexpected_id_stay` FOREIGN KEY (`id_stay`) REFERENCES `stay` (`id`);

--
-- Contraintes pour la table `cost`
--
ALTER TABLE `cost`
  ADD CONSTRAINT `FK_cost_id_service` FOREIGN KEY (`id_service`) REFERENCES `service` (`id`);

--
-- Contraintes pour la table `create_ticket`
--
ALTER TABLE `create_ticket`
  ADD CONSTRAINT `FK_create_ticket_id` FOREIGN KEY (`id`) REFERENCES `ticket` (`id`),
  ADD CONSTRAINT `FK_create_ticket_id_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`);

--
-- Contraintes pour la table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `FK_customer_id_stay` FOREIGN KEY (`id_stay`) REFERENCES `stay` (`id`);

--
-- Contraintes pour la table `customer_pro`
--
ALTER TABLE `customer_pro`
  ADD CONSTRAINT `FK_customer_pro_id` FOREIGN KEY (`id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `FK_customer_pro_id_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`);

--
-- Contraintes pour la table `date`
--
ALTER TABLE `date`
  ADD CONSTRAINT `FK_date_id` FOREIGN KEY (`id`) REFERENCES `menu_day` (`id`);

--
-- Contraintes pour la table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FK_employee_id_job` FOREIGN KEY (`id_job`) REFERENCES `job` (`id`);

--
-- Contraintes pour la table `ext_spa`
--
ALTER TABLE `ext_spa`
  ADD CONSTRAINT `FK_ext_spa_id` FOREIGN KEY (`id`) REFERENCES `booking_spa` (`id`);

--
-- Contraintes pour la table `ext_ts`
--
ALTER TABLE `ext_ts`
  ADD CONSTRAINT `FK_ext_ts_id` FOREIGN KEY (`id`) REFERENCES `booking_table_set` (`id`);

--
-- Contraintes pour la table `facturer`
--
ALTER TABLE `facturer`
  ADD CONSTRAINT `FK_facturer_id` FOREIGN KEY (`id`) REFERENCES `stay` (`id`),
  ADD CONSTRAINT `FK_facturer_id_facture` FOREIGN KEY (`id_facture`) REFERENCES `facture` (`id`);

--
-- Contraintes pour la table `int_spa`
--
ALTER TABLE `int_spa`
  ADD CONSTRAINT `FK_int_spa_id` FOREIGN KEY (`id`) REFERENCES `booking_spa` (`id`),
  ADD CONSTRAINT `FK_int_spa_id_stay` FOREIGN KEY (`id_stay`) REFERENCES `stay` (`id`);

--
-- Contraintes pour la table `int_ts`
--
ALTER TABLE `int_ts`
  ADD CONSTRAINT `FK_int_ts_id` FOREIGN KEY (`id`) REFERENCES `booking_table_set` (`id`),
  ADD CONSTRAINT `FK_int_ts_id_stay` FOREIGN KEY (`id_stay`) REFERENCES `stay` (`id`);

--
-- Contraintes pour la table `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `FK_job_id_service` FOREIGN KEY (`id_service`) REFERENCES `service` (`id`);

--
-- Contraintes pour la table `menu_day`
--
ALTER TABLE `menu_day`
  ADD CONSTRAINT `FK_menu_day_cle` FOREIGN KEY (`cle`) REFERENCES `date` (`cle`);

--
-- Contraintes pour la table `night`
--
ALTER TABLE `night`
  ADD CONSTRAINT `FK_night_id_breakfast` FOREIGN KEY (`id_breakfast`) REFERENCES `breakfast` (`id`),
  ADD CONSTRAINT `FK_night_id_room` FOREIGN KEY (`id_room`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_night_id_stay` FOREIGN KEY (`id_stay`) REFERENCES `stay` (`id`);

--
-- Contraintes pour la table `off`
--
ALTER TABLE `off`
  ADD CONSTRAINT `FK_off_id_room` FOREIGN KEY (`id_room`) REFERENCES `room` (`id`);

--
-- Contraintes pour la table `pour_des`
--
ALTER TABLE `pour_des`
  ADD CONSTRAINT `FK_pour_des_id` FOREIGN KEY (`id`) REFERENCES `menu` (`id`),
  ADD CONSTRAINT `FK_pour_des_id_booking_table_set` FOREIGN KEY (`id_booking_table_set`) REFERENCES `booking_table_set` (`id`);

--
-- Contraintes pour la table `pour_un`
--
ALTER TABLE `pour_un`
  ADD CONSTRAINT `FK_pour_un_id` FOREIGN KEY (`id`) REFERENCES `menu_day` (`id`),
  ADD CONSTRAINT `FK_pour_un_id_booking_table_set` FOREIGN KEY (`id_booking_table_set`) REFERENCES `booking_table_set` (`id`);

--
-- Contraintes pour la table `recipient_ticket`
--
ALTER TABLE `recipient_ticket`
  ADD CONSTRAINT `FK_recipient_ticket_id` FOREIGN KEY (`id`) REFERENCES `employee` (`id`),
  ADD CONSTRAINT `FK_recipient_ticket_id_ticket` FOREIGN KEY (`id_ticket`) REFERENCES `ticket` (`id`);

--
-- Contraintes pour la table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `FK_room_id_type_room` FOREIGN KEY (`id_type_room`) REFERENCES `type_room` (`id`);

--
-- Contraintes pour la table `salary`
--
ALTER TABLE `salary`
  ADD CONSTRAINT `FK_salary_id_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`);

--
-- Contraintes pour la table `stay`
--
ALTER TABLE `stay`
  ADD CONSTRAINT `FK_stay_id_agency` FOREIGN KEY (`id_agency`) REFERENCES `agency` (`id`),
  ADD CONSTRAINT `FK_stay_id_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`);

--
-- Contraintes pour la table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `FK_stock_id_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`),
  ADD CONSTRAINT `FK_stock_id_unit` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
